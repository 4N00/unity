﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slash: BaseAttack
{
    public Slash()
    {
        attackName = "Slash";
        attackDescription = "I didn't know you where a guns N' roses fan";
        attackDamage = 10f;
        attackCost = 0;
    }
}
