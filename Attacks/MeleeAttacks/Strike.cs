﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Strike : BaseAttack
{
    public Strike()
    {
        attackName = "Strike";
        attackDescription = "This is a strike, not a spare";
        attackDamage = 15f;
        attackCost = 0;
    }
}
