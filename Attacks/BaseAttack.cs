﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BaseAttack
{
    public string attackName;
    public string attackDescription;
    public float attackDamage; // Base damage 15
    public float attackCost; // Mana
}
