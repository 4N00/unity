﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStateMachine : MonoBehaviour
{
    private BattleStateMachine BSM;
    public BaseEnemy enemy;

    public enum TurnState
    {
        PROCESSING,
        CHOOSEACTION,
        WAITING,
        TAKEACTION,
        DEAD
    }

    public TurnState currentState;
    //for the ProgressBar
    private float cur_cooldown = 0f;
    private float max_cooldown = 10f;

    //this gameobject
    private Vector3 startposition;
    public GameObject Selector;
    //timeforaction stuff
    private bool actionStarted = false;
    public GameObject HeroToAttack;
    private float animSpeed = 5f;

    void Start()
    {
                currentState = TurnState.PROCESSING;
                Selector.SetActive(false);
                BSM = GameObject.Find("BattleManager").GetComponent<BattleStateMachine>();
                startposition = transform.position;
    }

    void Update()
    {
        // Debug.Log(currentState);
        switch (currentState)
        {
            
            case(TurnState.PROCESSING):
                UpgradeProgressBar();
            break;

            case(TurnState.CHOOSEACTION):
                ChooseAction();
                currentState = TurnState.WAITING;
            break;

            case(TurnState.WAITING):
 
            break;
                                    
            case(TurnState.TAKEACTION):
                StartCoroutine(TimeForAction());
            break;
                        
            case(TurnState.DEAD):

            break;
        }
    }

    void UpgradeProgressBar ()
    {
        cur_cooldown = cur_cooldown + Time.deltaTime;
        if(cur_cooldown >= max_cooldown)
        {
            currentState = TurnState.CHOOSEACTION;
        }
    }

    void ChooseAction ()
    {
        HandleTurn myAttack = new HandleTurn ();
        myAttack.Attacker = enemy.theName;
        myAttack.Type = "Enemy";
        myAttack.AttackersGameObject = this.gameObject;
        myAttack.AttackersTarget = BSM.HerosInBattle [Random.Range (0, BSM.HerosInBattle.Count)];
        BSM.CollectActions (myAttack);
    }

    private IEnumerator TimeForAction()
    {
        if(actionStarted)
        {
            yield break;
        }
        actionStarted = true;

        //animate the enemy near the hero to attack
        //Vector3 heroPosition = new Vector3(HeroToAttack.transform.position.x + 1.4f, HeroToAttack.transform.position.y + 0.8f, HeroToAttack.transform.position.z + 0f);
        //while (MoveTowardsEnemy(heroPosition)) { yield return null; }
        // //wait a bit
        yield return new WaitForSeconds(1f);
        // //do damage

        // //animate back to startposition
        Vector3 firstPosition = startposition;
        while (MoveTowardsStart(firstPosition)) { yield return null; }

        //remove this performer from the list in BSM
        BSM.PerformList.RemoveAt(0);
        //reset BSM
        BSM.battleStates = BattleStateMachine.PerformAction.WAIT;
        //end coroutine 
        actionStarted = false;
        //reset this enemey state
        cur_cooldown = 0f;
        currentState = TurnState.PROCESSING;
    }
    private bool MoveTowardsEnemy(Vector3 target)
    {
        return target != (transform.position = Vector3.MoveTowards(transform.position, target, animSpeed * Time.deltaTime));
    }

    private bool MoveTowardsStart(Vector3 target)
    {
        return target != (transform.position = Vector3.MoveTowards(transform.position, target, animSpeed * Time.deltaTime));
    }
}
