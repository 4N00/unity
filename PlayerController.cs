﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Copyright (C) Xenfinity LLC - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Bilal Itani <bilalitani1@gmail.com>, June 2017
 */

public class PlayerController : MonoBehaviour
{

    public float speed;

    private Rigidbody rb;
    private Animator anim;
    Vector3 characterScale;
    float characterScaleX;
    public bool isRunning;
    public bool isRolling;
    public ParticleSystem dust;
    #region Monobehaviour API

    void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        characterScale = transform.localScale;
        characterScaleX = characterScale.x;

    }

    void Update()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0, moveVertical);

        rb.AddForce(movement * speed);

        if (Input.GetAxis("Horizontal") < 0)
        {
            characterScale.x = -characterScaleX;

        }
        if (Input.GetAxis("Horizontal") > 0)
        {
            characterScale.x = characterScaleX;
        }
        transform.localScale = characterScale;

        if(moveHorizontal == 0)
        {
            anim.SetBool("isRunning", false);
        }
        else {
            anim.SetBool("isRunning", true);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            anim.SetBool("isRolling", true);
            rb.AddForce (movement * speed * 8);
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            CreateDust();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            CreateDust();
        }
    }

    void CreateDust()
    {
        dust.Play();
    }

    #endregion
}
